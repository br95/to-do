import Vue from "vue";

export function addUser(state, user) {
  Vue.set(state.users, user.uid, user.data);
}

export function deleteUser(state, uid) {
  Vue.delete(state.users, uid);
}

export function updateUser(state, user) {
  Object.assign(state.users[user.uid], user.updates);
}
