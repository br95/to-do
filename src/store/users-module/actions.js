import {
  usersCollection,
  getUserFirebase
} from "../../firebase/firebaseConfig.js";

export const registerUser = async ({ commit }, user) => {
  try {
    const alreadyRegisteredUser = await getUserFirebase(user.uid);
    // if the user is already added to the firebase
    if (alreadyRegisteredUser.exists) {
      let userData = alreadyRegisteredUser.data();
      userData.lastLoginAt = new Date().getTime();
      commit("addUser", {
        uid: alreadyRegisteredUser.id,
        data: userData
      });
      return;
    }
    const addedUser = await usersCollection.doc(user.uid).set({ ...user.data });
    commit("addUser", { uid: user.uid, data: user.data });
    console.log("Added user successfuly in firebase");
  } catch (error) {
    console.error("Error adding user: ", error);
  }
};

export const loadRegisteredUser = async ({ commit }, userId) => {
  try {
    const fbUser = await getUserFirebase(userId);
    let userObject = { uid: userId, data: fbUser.data() };
    commit("addUser", userObject);
    console.log("Uspjesno loadovan registrovani korisnik");
  } catch (error) {
    console.error(
      "Greska prilikom loadovanja registrovanog korisnika: " + error
    );
  }
};

export const updateUser = async ({ commit }, user) => {
  commit("updateUser", user);
  try {
    await usersCollection.doc(user.uid).update(user.updates);
    console.log("Updated user successfuly in firebase");
  } catch (error) {
    console.error("Error updating user in firebase", error);
  }
};

export const deleteUser = async ({ commit }, uid) => {
  commit("deleteUser", uid);
};
