import { tasksCollection } from "../../firebase/firebaseConfig.js";
import { date } from "quasar";

export const updateTask = async ({ commit }, payload) => {
  commit("updateTask", payload);
  try {
    await tasksCollection.doc(payload.id).update(payload.updates);
    console.log("Updated task successfuly in firebase");
  } catch (error) {
    console.error("Error updating task in firebase", error);
  }
};

export const deleteTask = async ({ commit }, id) => {
  commit("deleteTask", id);
  try {
    await tasksCollection.doc(id).delete();
    console.log("Deleted task succesfuly in firebase");
  } catch (error) {
    console.error("Error removing task in firebase: ", error);
  }
};

export const addTask = async ({ commit }, task) => {
  try {
    const addedTask = await tasksCollection.add(task);
    console.log("Added task successfuly in firebase");
    commit("addTask", { id: addedTask.id, data: task });
  } catch (error) {
    console.error("Error adding task: ", error);
  }
};

export const loadTasks = async (context, user) => {
  if (user === null || user === undefined) return;
  try {
    const querySnapshot = await tasksCollection
      .where("userId", "==", user.uid)
      .get();
    let usersTasks = {};
    querySnapshot.forEach((task) => {
      if (task.data().completed && isForDeletion(task.data())) {
        deleteTask(context, task.id);
      } else {
        usersTasks[task.id] = task.data();
      }
    });
    console.log("Successfully loaded tasks");
    context.commit("loadTasks", usersTasks);
  } catch (error) {
    console.error("Error loading tasks: ", error);
  }
};

// Checks if a task is finished more than a day so we can remove it
const isForDeletion = (task) => {
  let afterFinish = date.getDateDiff(new Date(), new Date(task.finishedDate));
  console.log(afterFinish);
  return afterFinish > 1;
};

export const clearTasks = ({ commit }) => {
  commit("clearTasks");
};
