import Vue from "vue";

// Checks tasks and makes it finished
export function updateTask(state, payload) {
  Object.assign(state.tasks[payload.id], payload.updates);
}

//Deletes task from state
export function deleteTask(state, id) {
  Vue.delete(state.tasks, id);
}

export function addTask(state, task) {
  Vue.set(state.tasks, task.id, task.data);
}

export function editTask(state, task) {
  Object.assign(state[task.id], task.data);
}

export function loadTasks(state, tasks) {
  state.tasks = tasks;
}

export function clearTasks(state) {
  state.tasks = {};
}
