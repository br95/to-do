export const tasks = state => {
  let resultTasks = {};
  Object.keys(state.tasks).forEach(id => {
    let currentTask = state.tasks[id];
    if (!currentTask.completed) {
      resultTasks[id] = currentTask;
    }
  });
  return resultTasks;
};

export const completedTasks = state => {
  let resultTasks = {};
  Object.keys(state.tasks).forEach(id => {
    let currentTask = state.tasks[id];
    if (currentTask.completed) {
      resultTasks[id] = currentTask;
    }
  });
  return resultTasks;
};
