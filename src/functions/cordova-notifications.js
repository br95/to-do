import { dateTimeObject } from "./datefunctions";

const scheduleNotification = notification => {
  cordova.plugins.notification.local.schedule(notification, success => {
    if (success) {
      console.log("Uspjesno zakazana jos jedna notifikacija:");
      console.log(notification);
    } else {
      console.error(
        "Something went wrong while trying to schedule notification: " +
          notification
      );
    }
  });
};

const scheduleMultipleNotifications = notifications => {
  cordova.plugins.notification.local.schedule(notifications, success => {
    if (success) {
      console.log("Uspjesno zakazane notifikacije:");
      console.log(notifications);
      // Dodaje listenere kad zakaze notifikacije
      confirmNotification();
      snoozeNotification();
      onUpdateNotificationEvent();
      setDefaultNotificationSettings();
      onScheduleNotificationEvent();
      onTriggerNotificationEvent();
    } else {
      console.error("Notifikacije nije moguce zakazati!");
    }
  });
};

const cancelNotification = id => {
  cordova.plugins.notification.local.cancel(id, result => {
    if (result === "OK") {
      console.log("Successfully canceled notification with id: " + id);
    } else {
      console.error(
        "Something went wrong when tried to cancel notification with id: " + id
      );
    }
  });
};

const confirmNotification = () => {
  cordova.plugins.notification.local.on("yes", (notification, eopts) => {
    cancelNotification(notification.id);
  });
};

const snoozeNotification = () => {
  cordova.plugins.notification.local.on("no", (notification, eopts) => {
    //reschedule notification as new notification with timing set to be every 2 hours for now its fixed
    // TEST: every minute *
    let notificationData = notification.data;
    let newNotif = { ...notification };
    let newNotifDate = new Date(parseInt(notificationData));
    newNotifDate.setMinutes(newNotifDate.getMinutes() + 1);
    newNotif.id = parseInt(Math.random() * 1000) * 371;
    newNotif.trigger.at = newNotifDate.getTime();
    newNotif.data = newNotifDate.getTime();
    cancelNotification(notification.id);
    scheduleNotification(newNotif);
  });
};

const taskNotificationObject = (task, notifId) => {
  //Function which returns object formated with separated properties of date values(hour,minute,day...)
  let dt = dateTimeObject(task.dueDate, task.dueTime);
  dt.day = dt.day - 1;
  let taskDateTime = new Date(dt.year, dt.month, dt.day, dt.hour, dt.minute);
  let atTheMomment = new Date();
  let isPassedDeadline = "Deadline: ";
  //if its passed the notification deadline
  if (taskDateTime.getDate() + 1 <= atTheMomment.getDate()) {
    taskDateTime.setDate(atTheMomment.getDate());
    taskDateTime.setHours(atTheMomment.getHours());
    taskDateTime.setMinutes(atTheMomment.getMinutes());
    taskDateTime.setSeconds(atTheMomment.getSeconds() + 20);
    isPassedDeadline = "Deadline missed for this task! ";
  }
  let notifObject = {
    id: notifId,
    led: "#ff6501",
    color: "#efb85d",
    title: task.name,
    text: `${isPassedDeadline} ${task.dueDate} - ${task.dueTime} \n ${task.details}`,
    foreground: true,
    trigger: {
      at: taskDateTime.getTime()
    },
    actions: [
      { id: "yes", title: "Got it!" },
      { id: "no", title: "Snooze" }
    ],
    data: taskDateTime.getTime(),
    icon:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTzfXKe6Yfjr6rCtR6cMPJB8CqMAYWECDtDqH-eMnerHHuXv9egrw"
  };
  return notifObject;
};

const updateNotification = notificationDTO => {
  console.log(notificationDTO);
  cordova.plugins.notification.local.update(notificationDTO, success => {
    if (success) {
      console.log("Uspjesno updejtovana notifikacija!");
    }
  });
};

const onTriggerNotificationEvent = () => {
  cordova.plugins.notification.local.on("trigger", notification => {
    console.log("Successfully fired trigger event");
    console.log(notification);
  });
};

const onUpdateNotificationEvent = () => {
  cordova.plugins.notification.local.on("update", notification => {
    console.log("Succesfully fired update event");
    console.log(notification);
  });
};

const onScheduleNotificationEvent = () => {
  cordova.plugins.notification.local.on("schedule", notification => {
    console.log("Successfully fired schedule event");
    console.log(notification);
  });
};
const setDefaultNotificationSettings = settings => {
  cordova.plugins.notification.local.setDefaults(settings);
};

export {
  scheduleNotification,
  scheduleMultipleNotifications,
  confirmNotification,
  snoozeNotification,
  setDefaultNotificationSettings,
  onScheduleNotificationEvent,
  onUpdateNotificationEvent,
  taskNotificationObject,
  updateNotification
};
