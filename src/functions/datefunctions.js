const getDateNow = () => {
  //Format: dd/MM/YYYY
  let temp = new Date().toLocaleString().split(",")[0];
  //temp = (M || MM)/(d || dd)/YYYY
  let result = "";
  // to know when to add leading 0 for days
  if (parseInt(temp.substr(2, 1)) < 10 && temp.substr(3, 1) === "/") {
    result += "0" + temp.substr(2, 1);
  } else {
    result += "" + temp.substr(2, 2);
  }
  // to know when to add leading zero for months
  if (parseInt(temp.substr(0, 1)) < 10) {
    result += "/" + "0" + temp.substr(0, 1);
  } else {
    result += "/" + temp.substr(0, 2);
  }
  // years allways at the end
  result += "/" + temp.substr(4, 6);
  return result;
};

const dateTimeObject = (date, time) => {
  let day = parseInt(date.substr(0, 2));
  let month = parseInt(date.substr(3, 2)) - 1;
  let year = parseInt(date.substr(6, 4));
  let hour = parseInt(time.substr(0, 2));
  let minute = parseInt(time.substr(3, 2));
  const dateTime = {
    day,
    month,
    year,
    hour,
    minute
  };
  return dateTime;
};

export { getDateNow, dateTimeObject };
